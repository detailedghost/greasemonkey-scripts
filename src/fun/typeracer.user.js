// ==UserScript==
// @name        Re-enter death mode
// @namespace   fun
// @match       https://play.typeracer.com/?universe=accuracy
// @grant       MIT
// @version     1.0
// @author      Dante Burgos
// @description Auto resets auto death mode
// ==/UserScript==

const obs = new MutationObserver((records, _obs) => {
  for (const rec of records.filter(r => r.type === "childList" && r.target.classList.contains("redesign") && r.addedNodes.length > 0)) {
    const popup = Array.from(rec.addedNodes).filter(n => n.classList.contains('InstaGibPopup')).at(0);
    if (!popup) continue;

    const closeBtn = Array.from(popup.children[0].children[0].children).filter(c => c.classList.contains('xButton')).at(0);
    console.log(closeBtn)
    if (!closeBtn) continue;

    closeBtn.click();
    document.querySelector("#gwt-uid-2 > a").click()
  }
}).observe(document.querySelector("body.redesign"), {
  childList: true,
  subtree: true,
})
