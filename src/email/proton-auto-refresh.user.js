// ==UserScript==
// @name        Auto refresh proton mailbox
// @namespace   email
// @match       https://mail.proton.me/u/*/inbox
// @grant       MIT
// @version     1.0
// @author      Dante Burgos
// @description Auto refresh Proton Mailbox
// ==/UserScript==

setInterval(() => {
  location.reload();
}, 1000 * 60 * 15)
