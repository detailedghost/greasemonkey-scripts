// ==UserScript==
// @name        Zoom - Always join by audio
// @namespace   video
// @match       https://*.zoom.us/*/join
// @grant       MIT
// @version     1.1
// @author      Dante Burgos
// ==/UserScript==

const zoom_join_audio_interval = setInterval(() => {
  const btn = document.querySelector("#voip-tab > div > button");
  if (btn) {
    btn.click();
    clearInterval(zoom_join_audio_interval);
  }
}, 1000 * 1);
