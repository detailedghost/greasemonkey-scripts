// ==UserScript==
// @name        Zoom - Auto emote once
// @namespace   video
// @match       https://*.zoom.us/*/join
// @grant       MIT
// @version     1.0
// @author      Dante Burgos
// @description 2/9/2023, 9:45:39 AM
// ==/UserScript==

const EMOJI_CODE = ":wave:";

const emoji_interval = setInterval(() => {
  const reactionBtn = document.querySelector(
    "#foot-bar > div.footer__btns-container > div:nth-child(5) > div > div > button"
  );
  if (reactionBtn) {
    reactionBtn.click();
    document
      .querySelector(
        "#reactionPicker > div > div:nth-child(3) > div > div:nth-child(1) > button:nth-child(7) > div > div"
      )
      .click();
    document.querySelector(`button[title="${EMOJI_CODE}"]`)?.click();
    clearInterval(emoji_interval);
  }
}, 1000 * 3);
