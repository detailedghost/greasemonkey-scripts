// ==UserScript==
// @name        Zoom - Always Browser
// @namespace   video
// @match       https://*zoom.us/j/*
// @grant       MIT
// @version     1.0
// @author      Dante Burgos
// @description Always start Zoom In the browser
// ==/UserScript==


(function () {
  const i = setInterval(() => {
    const joinLink = document.querySelector("#zoom-ui-frame a[web_client]");
    if (joinLink) {
      joinLink.click();
      clearInterval(i);
    }
  }, 1 * 1000);
})();
