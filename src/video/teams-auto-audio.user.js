// ==UserScript==
// @name        Microsoft Teams - Auto join, with mute mic + open chat
// @namespace   video
// @match       https://teams.microsoft.com/*
// @grant       MIT
// @version     1.0
// @author      Dante Burgos
// @description 2/9/2023, 9:45:39 AM
// ==/UserScript==

document.onreadystatechange = () => {
  const EMOJI = "surprised";
  const MIC_STATE = "muted"; // or 'open'

  function toggleMuteBtn() {
    const radio = document.querySelector("div[aria-label='Computer audio']");
    if (!radio) return;
    radio.click();

    const muteSection = document.querySelector("div[data-tid=toggle-mute]");
    if (!muteSection) return;

    const isMuted = muteSection.dataset.cid === "toggle-mute-false";

    if (MIC_STATE === "muted" && !isMuted) {
      muteSection.children[1].click();
    }
    if (MIC_STATE === "open" && isMuted) {
      muteSection.children[1].click();
    }

    document.querySelectorAll(".ui-checkbox__indicator")[1].click(); // toggle mute
    document.querySelector("#prejoin-join-button > span").click(); // join
  }

  function openChat() {
    const chatIcon = document.querySelectorAll(".ui-toolbar__itemicon")[0];
    if (!chatIcon) return;
    chatIcon.click();
  }

  function triggerEmoji() {
    const picker = document.querySelector('button[data-tid="newMessageCommands-EmoticonPicker"]');
    if (!picker) return;
    picker.click();
    document.querySelector(`img[itemid="${EMOJI}"]`)?.click();
    document.querySelector('button[data-tid="newMessageCommands-send"]')?.click();
  }

  const obs = new MutationObserver((list, _ob) => {
    toggleMuteBtn();
  });

  const attempt = setInterval(() => {
    if (location.href.indexOf("modern-calling") < 0) return;
    const app = document.getElementById("wrapper");
    if (!app) return;
    obs.observe(app, { childList: true });
    clearInterval(attempt);
  }, 1_000);
};
