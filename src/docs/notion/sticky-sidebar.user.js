// ==UserScript==
// @name        Notion - Sticky Sidebar
// @namespace   docs
// @match       https://www.notion.so/*
// @grant       MIT
// @version     1.0
// @author      Dante Burgos
// @description 3/1/2023, 9:59:11 AM
// ==/UserScript==

document.onreadystatechange = function () {
  function handle(_list, _observer) {
    const toc = document.querySelector(".notion-table_of_contents-block");
    if (!toc) return;
    if (toc.parentElement.style.position === "sticky") return;
    if (toc.closest(".notion-column-block") !== null) {
      let origCss = toc.parentElement.style.cssText;
      toc.parentElement.style.cssText = `${origCss} position:sticky; top: 0; align-self: flex-start;`;
    }
  }
  const observer = new MutationObserver(handle);

  const attempt_interval = setInterval(() => {
    const notion_elem = document.querySelector(".notion-frame");
    if (!notion_elem) return;
    observer.observe(notion_elem, {
      childList: true,
    });

    clearInterval(attempt_interval);
  }, 500);
};
