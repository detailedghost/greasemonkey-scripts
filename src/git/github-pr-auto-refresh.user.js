// ==UserScript==
// @name        Github Auto Refresh PR
// @namespace   git
// @match       https://github.com/*/pull*
// @grant       MIT
// @version     1.0
// @author      Dante Burgos
// @description When Github PR has refresh button appear, automatically click it.
// ==/UserScript==

(() => {
  let i;
  const clickRefresh = () => {
    const refreshBtn = document.querySelector(".stale-files-tab-link");
    if (!refreshBtn) {
      console.debug("no refresh found, continuing");
      i = setTimeout(clickRefresh, 1 * 1000);
      return;
    }
    refreshBtn.click();
    clearTimeout(i);
  };

  clickRefresh();
})();
